def count_long_subarray(A):
    '''
    Input:  A     | Python Tuple of positive integers
    Output: count | number of longest increasing subarrays of A
    '''
    count = 0
    ##################
    # YOUR CODE HERE #
    ##################
    max_length = 1
    current_length = 1
    last = None

    for i in range(len(A)):
        if last is not None:
            if A[i] > last:
                current_length = current_length + 1
            if A[i] <= last or i + 1 == len(A):
                if current_length > max_length:
                    count = 1
                    max_length = current_length
                elif current_length == max_length:
                    count = count + 1
                current_length = 1
        last = A[i]

    return count
